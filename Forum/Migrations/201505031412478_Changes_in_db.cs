namespace Forum.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Changes_in_db : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Categories", "Comment", c => c.String());
            AlterColumn("dbo.Topics", "Author", c => c.String());
            AlterColumn("dbo.Posts", "Author", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Posts", "Author", c => c.Int(nullable: false));
            AlterColumn("dbo.Topics", "Author", c => c.Int(nullable: false));
            DropColumn("dbo.Categories", "Comment");
        }
    }
}
