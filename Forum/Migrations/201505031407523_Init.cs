namespace Forum.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Posts", "Author", c => c.Int(nullable: false));
            AlterColumn("dbo.Topics", "Author", c => c.Int(nullable: false));
            DropColumn("dbo.Categories", "Comment");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Categories", "Comment", c => c.String());
            AlterColumn("dbo.Topics", "Author", c => c.String());
            AlterColumn("dbo.Posts", "Author", c => c.String());
        }
    }
}
