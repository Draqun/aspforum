namespace Forum.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Parameters : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Categories", "Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Categories", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Categories", "Comment", c => c.String(nullable: false));
            AlterColumn("dbo.Topics", "Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Topics", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Topics", "Author", c => c.String(nullable: false));
            AlterColumn("dbo.Posts", "Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Posts", "Context", c => c.String(nullable: false));
            AlterColumn("dbo.Posts", "Author", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Posts", "Author", c => c.String());
            AlterColumn("dbo.Posts", "Context", c => c.String());
            AlterColumn("dbo.Posts", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Topics", "Author", c => c.String());
            AlterColumn("dbo.Topics", "Name", c => c.String());
            AlterColumn("dbo.Topics", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Categories", "Comment", c => c.String());
            AlterColumn("dbo.Categories", "Name", c => c.String());
            AlterColumn("dbo.Categories", "Id", c => c.Int(nullable: false, identity: true));
        }
    }
}
