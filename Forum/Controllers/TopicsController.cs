﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Forum.Models;

namespace Forum.Controllers
{
    public class TopicsController : Controller
    {
        private TopicsContext db = new TopicsContext();

        //
        // GET: /Topics/

        public ActionResult Index(int CategoryId)
        {
            var topics = db.Topics.Include(t => t.Category).Where( t => t.CategoryId == CategoryId);
            ViewBag.CategoryId = CategoryId;
            ViewBag.CategoryName = db.Categories.Find(CategoryId).Name;
            return View(topics.ToList());
        }

        //
        // GET: /Topics/Create
        [Authorize]
        public ActionResult Create(int CategoryId)
        {
            ViewBag.CategoryId = CategoryId;
            return View();
        }

        //
        // POST: /Topics/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create(Topic topic, int CategoryId)
        {
            if (ModelState.IsValidField("Name"))
            {
                topic.Author = User.Identity.Name;
                topic.CategoryId = CategoryId;
                db.Topics.Add(topic);
                db.SaveChanges();
                return RedirectToAction("Index", "Topics", new { CategoryId = CategoryId });
            }

            ViewBag.CategoryId = CategoryId;
            ViewBag.CategoryName = db.Categories.Find(CategoryId).Name;
            return View(topic);
        }

        //
        // GET: /Topics/Edit/5
        [Authorize]
        public ActionResult Edit(int id, int CategoryId)
        {
            Topic topic = db.Topics.Find(id);
            if (topic == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryId = CategoryId;
            return View(topic);
        }

        //
        // POST: /Topics/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit(Topic topic, int CategoryId)
        {
            if (ModelState.IsValidField("Name"))
            {
                topic.Author = User.Identity.Name;
                topic.CategoryId = CategoryId;
                db.Entry(topic).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { CategoryId = CategoryId });
            }
            ViewBag.CategoryId = CategoryId;
            return View(topic);
        }

        //
        // GET: /Topics/Delete/5
        [Authorize(Roles="Admin")]
        public ActionResult Delete(int id = 0)
        {
            Topic topic = db.Topics.Find(id);
            if (topic == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryId = topic.CategoryId;
            return View(topic);
        }

        //
        // POST: /Topics/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles="Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Topic topic = db.Topics.Find(id);
            int CategoryId = topic.Category.Id;
            db.Topics.Remove(topic);
            db.SaveChanges();
            return RedirectToAction("Index", new { CategoryId = CategoryId });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}